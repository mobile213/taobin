import 'dart:io';

class Taobin {
  late final int price;
  void showType() {
    String type = '''
  ---------------------
  Select Type

    1. Coffee
    2. Tea
    3. Milk and Cocoa
    4. Soda
  Your Choice ? 
  ''';

    return stdout.write(type);
  }

  void showMenu(int choose) {
    if (choose == 1) {
      String Menu = '''
  ---------------------
  Type: Coffee
  Select Menu
    1. เอสเพรสโซ่
    2. ลาเต้เย็น
    3. มอคค่าเย็น 
    4. อเมริกาโน่
    5. คาปูชิโน่
    6. กาแฟบราวน์ชูก้าร์
  You Choose ?  
  ''';
      return stdout.write(Menu);
    } else if (choose == 2) {
      String Menu = '''
  ---------------------
  Type: Tea
  Select Menu
    1. ชาไทย ชานมเย็น
    2. ชานมไต้หวัน
    3. ชาบราวน์ชูการ์
    4. ชามะนาว
    5. มัทฉะลาเต้เย็น และ ชาเขียวญี่ปุ่นเย็น
  Your Choose ? 
  ''';
      return stdout.write(Menu);
    } else if (choose == 3) {
      String Menu = '''
  ---------------------
  Type: Milk and Cocoa
  Select Menu
    1. โอริโอ้ปั่นภูเขาไฟ
    2. โกโก้เย็น
    3. นมโคคุโตะ
    4. นมชมพูเย็น
  Your Choose ? 
  ''';
      return stdout.write(Menu);
    } else if (choose == 4) {
      String Menu = '''
  ---------------------
  Type: Soda
  Select Menu
    1. มะนาวโซดา
    2. ลิ้นจี่โซดา
    3. น้ำแดงโซดา
    4. บ๊วยมะนาวโซดา
  Your Choose ? 
  ''';
      return stdout.write(Menu);
    }
  }

  void printMenuPrice(chooseType, chooseMenu) {
    if (chooseType == 1) {
      if (chooseMenu == 1) {
        print("--You Choose เอสเพรสโซ่ 35฿--");
        price = 35;
      } else if (chooseMenu == 2) {
        print("--You Choose ลาเต้เย็น 45฿--");
        price = 45;
      } else if (chooseMenu == 3) {
        print("--You Choose มอคค่าเย็น 45--฿");
        price = 45;
      } else if (chooseMenu == 4) {
        print("--You Choose อเมริกาโน่ 40--฿");
        price = 40;
      } else if (chooseMenu == 5) {
        print("--You Choose คาปูชิโน่ 45฿--");
        price = 45;
      } else if (chooseMenu == 6) {
        print("--You Choose กาแฟบราวน์ชูก้าร์ 50฿--");
        price = 50;
      }
    } else if (chooseType == 2) {
      if (chooseMenu == 1) {
        print("--You Choose ชาไทย 35฿--");
        price = 35;
      } else if (chooseMenu == 2) {
        print("--You Choose ชานมไต้หวัน 40฿--");
        price = 40;
      } else if (chooseMenu == 3) {
        print("--You Choose ชาบราวน์ชูการ์ 35฿--");
        price = 35;
      } else if (chooseMenu == 4) {
        print("--You Choose ชามะนาว 20฿--");
        price = 20;
      } else if (chooseMenu == 5) {
        print("--You Choose มัทฉะเย็น 45฿--");
        price = 45;
      }
    } else if (chooseType == 3) {
      if (chooseMenu == 1) {
        print("--You Choose โกโก้ร้อน 35฿--");
        price = 35;
      } else if (chooseMenu == 2) {
        print("--You Choose โกโก้เย็น 35฿--");
        price = 35;
      } else if (chooseMenu == 3) {
        print("--You Choose นมโคคุโตะ 40--฿");
        price = 40;
      } else if (chooseMenu == 4) {
        print("--You Choose นมชมพูเย็น 40฿--");
        price = 40;
      }
    } else if (chooseType == 4) {
      if (chooseMenu == 1) {
        print("--You Choose มะนาวโซดา 20฿--");
        price = 20;
      } else if (chooseMenu == 2) {
        print("--You Choose ลิ้นจี่โซดา 20฿--");
        price = 20;
      } else if (chooseMenu == 3) {
        print("--You Choose มะม่วงโซดา 40฿--");
        price = 40;
      } else if (chooseMenu == 4) {
        print("--You Choose บ๊วยมะนาวโซดา 40฿--");
        price = 40;
      }
    }
  }

  void pay(money) {
    while (money < price) {
      print("*** not enough money ***");
      print("Please enter payment amount again");
      money = int.parse(stdin.readLineSync()!);
      ;
    }
    if (money > price) {
      print('Get change : ${money - price}');
    }
  }
}
