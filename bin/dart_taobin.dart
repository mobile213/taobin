import 'package:taobin/taobin.dart' as taobin;
import 'Taobin.dart';
import 'dart:io';

void main(List<String> arguments) {
  var taobin = new Taobin();
  print("Welcome to Taobin beverage");

  taobin.showType();
  var chooseType = int.parse(stdin.readLineSync()!);
  taobin.showMenu(chooseType);
  var chooseMenu = int.parse(stdin.readLineSync()!);
  taobin.printMenuPrice(chooseType, chooseMenu);

  print("Please enter payment amount ");
  var money = int.parse(stdin.readLineSync()!);
  taobin.pay(money);
}
